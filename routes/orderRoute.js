const orderController = require('../controllers/orderController');
const express = require('express');
const router = express.Router();
const auth = require('../auth');


// add to cart
// router.post('/cart', auth.verify, (req,res)=>{
// 	let data ={
// 		order: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		userId: auth.decode(req.headers.authorization).id
// 	}
// 	orderController.addToCart(data).then(result =>res.send(result));
// })

// add to cart 
router.post('/:productId/checkout', auth.verify, (req,res)=>{
	let data ={
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	orderController.order(req.params,data).then(result =>res.send(result));
})

// get all orders (ADMIN)
router.get('/', auth.verify, (req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.getAllOrders(data).then(result=>res.send(result))
})

module.exports = router;
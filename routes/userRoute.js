const express = require('express');

const router = express.Router();

const userController = require('../controllers/userController');

const auth = require('../auth');

// check user exists in the database
router.post('/check-user', (req,res)=>{
	userController.checkUser(req.body).then(result=>res.send(result))
})

//User registration
router.post('/register', (req,res)=>{
	userController.registerUser(req.body).then(result =>res.send(result));
})

// User login/authentication
router.post('/login', (req,res)=>{
	userController.loginUser(req.body).then(result =>res.send(result));
})

// retrieve user details
router.post('/details', auth.verify,(req,res)=>{
	const userData= auth.decode(req.headers.authorization)

	userController.getUser({userId: userData.id}).then(result =>res.send(result));
})

// retrieve logged user details
router.get('/details', auth.verify,(req,res)=>{
	const userData= auth.decode(req.headers.authorization)
	userController.getUser({userId: userData.id}).then(result =>res.send(result));
})

// set user as admin
router.put('/:userId/set-user',auth.verify,(req,res)=>{
	let data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUser(req.params,data).then(result =>res.send(result));
})

// retrieve logged user's order/s
router.get('/orders', auth.verify, (req,res)=>{
	const userId = auth.decode(req.headers.authorization).id
	userController.getOrders(userId).then(result=>{
		res.send(result)
	})
})

// retrieve all user
router.get('/', auth.verify, (req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllUsers(data).then(result=>res.send(result))
})


module.exports = router
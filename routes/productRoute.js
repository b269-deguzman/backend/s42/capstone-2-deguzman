const productController = require('../controllers/productController');

const express = require('express');
const router = express.Router();

const auth = require('../auth');

// create product
router.post('/create', auth.verify, (req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(result =>res.send(result));
})

// retrieve all active products
router.get('/active', (req,res)=>{
	productController.getAllActiveProducts().then(result => res.send(result));
} )

// retrieve single product
router.get('/:productId', (req,res)=>{
	productController.getProduct(req.params).then(result => res.send(result));
})

// retrieve all products
router.get('/',(req,res)=>{
	productController.getAllProducts().then(result => res.send(result));
})

// update product info
router.put('/:productId/update', auth.verify, (req,res)=>{
	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params,data).then(result => res.send(result));
})

// archive product
router.patch('/:productId/archive', auth.verify, (req,res)=>{
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, data).then(result =>{
		res.send(result)
	})
})

// activate product
 router.patch('/:productId/activate', auth.verify, (req,res)=>{
 	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.activateProduct(req.params, data).then(result =>{
		res.send(result) 
 })
})

module.exports = router;
const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User')

// create order
module.exports.order = async(reqParams,data)=>{
	if(!data.isAdmin){

		let newOrder = new Order({
					userId: data.userId
					})
					
		let isProductUpdated = await Product.findById(reqParams.productId).then(product =>{
			console.log(product)

				newOrder.products.push({productId: reqParams.productId, quantity: data.order.quantity, productName: product.name})

			newOrder.totalAmount = product.price * data.order.quantity
			return newOrder.save().then((order, err)=>{
				if(err){
					return false;
				}else{
					return true;
				}
			})
		
			

		})
		if(isProductUpdated){
			return true;
		}else{
			return false;
		}
	}
	let message = Promise.resolve('User must not be ADMIN to create order');
 	return message.then((value)=>{
 		return {value};
 	})

}



module.exports.getAllOrders = (data)=>{
	if(data.isAdmin){
		return Order.find().then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	}
	let message = Promise.resolve('User must not be ADMIN to get all orders');
 	return message.then((value)=>{
 		return {value};
 	})
}


const User= require("../models/User");
const Order = require('../models/Order')
const bcrypt = require("bcrypt");
const auth = require('../auth');

// check if user already exists
module.exports.checkUser=(reqBody)=>{
	const {email, userName, mobileNo} = reqBody

	return User.find({$or: [{email}, {userName}, {mobileNo}]})
	.then((user, error)=>{
		console.log(user)
		if(user.length > 0){
			return user
		}else{
			return false
		}
	})
}

// register user
module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		userName: reqBody.userName,
		password : bcrypt.hashSync(reqBody.password, 7)
	})

	return newUser.save().then((user, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// User login/authentication
module.exports.loginUser= (reqBody)=>{

	return User.findOne({userName: reqBody.userName}).then(registeredUser=>{
		if(registeredUser == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, registeredUser.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(registeredUser) }
			}else{
				return false;
			}
		}
	})
}

// retrieve user details
module.exports.getUser = (data)=>{
	return User.findById(data.userId).then(result=>{
		if(result == null){
			return false
		}else{
			result.password = ""
			return result
		}
	})
}

// set user as admin
module.exports.setUser = (reqParams,data)=>{
	if(data.isAdmin){
		let updatedUser = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user,err)=>{
				if(err){
					return false
				}else{
					let message = `${user.userName} is now set to admin.`
					return message;
				}
			})
	}
	let message = Promise.resolve('User must be ADMIN to set user');
	return message.then(result =>{
		return {orders: result}
	})
}

// retrieve authenticated user's orders
module.exports.getOrders =(data)=>{
	return Order.findById({userId: data}).then((result)=>{
		return result;
	})
}

// retrieve all users
module.exports.getAllUsers = (data)=>{
	if(data.isAdmin){
		return User.find().then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	}
	let message = Promise.resolve('User must be ADMIN to retrieve users');
 	return message.then((value)=>{
 		return {value};
 	})
}

const Product = require('../models/Product');

//create product

module.exports.createProduct = (data)=>{	
 	if(data.isAdmin){
	 	let newProduct = new Product({
	 		name: data.product.name,
	 		description: data.product.description,
	 		price: data.product.price
		 })
	 return newProduct.save().then((product, err)=>{
	 	if(err){
	 		return false;
	 	}else{
	 		return true;
	 	}
		 })
	 }
 	let message = Promise.resolve('User must be ADMIN to create product');
 	return message.then((value)=>{
 		return {value};
 	})
 }	

 // retrieve all active products
module.exports.getAllActiveProducts = ()=>{
	return Product.find({isActive: true}).then(result=>{
		return result;
	})
}

// retrieve single product
module.exports.getProduct =(reqParams)=>{
	let product = Product.findById(reqParams.productId)
	return product.then((result, err)=>{
			return result;
	})
}

// retrieve all products
module.exports.getAllProducts=(data)=>{
	 return Product.find({}).then((result, err)=>{
		if(err){
			return false
		}else{
			return result
		}
		})
	}
	


// update product info
module.exports.updateProduct = (reqParams,data)=>{
	if(data.isAdmin){
		let updateProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((result,err)=>{
			if(err){
				return false;
			}else{
				let message = `${result._id} is updated!`
				return {product: result, message: message};
			}
		})
	}
	let message = Promise.resolve('User must be ADMIN to update product');
	return message.then(result =>{
		return {result}
	})
}

// archive product
module.exports.archiveProduct = (reqParams, data)=>{
	if(data.isAdmin){
			let archivedProduct = {
			isActive: false
		}
			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product,err)=>{
				if(err){
					return false
				}else{
					let message = `${product} is now stored in archived products.`
					return {product: product, message: message};
				}
			})
		}
		let message = Promise.resolve('User must be ADMIN to archive product');
		return message.then(result =>{
		return {result}	
	})
}

// activate product
module.exports.activateProduct = (reqParams,data)=>{
	if(data.isAdmin){
		let activatedProduct = {
			isActive: true
		}
		return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((product,err)=>{
				if(err){
					return false
				}else{
					let message = `${product} is now activated.`
					return {product: product, message: message};
				}
			})
	}
	let message = Promise.resolve('User must be ADMIN to archive product');
		return message.then(result =>{
		return {result}	
	})
}


const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoute = require('./routes/userRoute');
const productRoute= require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute');
// Create application
const app= express();

// Middlewares
app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/orders', orderRoute);

// Database Connection
mongoose.connect("mongodb+srv://justinedeguzman279:admin123@zuitt-bootcamp.qulynt4.mongodb.net/capstoneproject2?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.on('error', console.error.bind(console, 'connection error:'))
mongoose.connection.once("open",()=>{
	console.log('Connected to cloud database.')
});

// Server listening
app.listen(process.env.PORT || 5000, ()=> console.log(`Application is connected to port ${process.env.PORT || 5000}`));